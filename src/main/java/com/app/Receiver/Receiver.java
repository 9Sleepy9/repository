package com.app.Receiver;

import com.app.Domain.Client;
import com.app.Sender.Sender;
import com.app.Service.ClientService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import static java.lang.Integer.parseInt;

@RabbitListener(queues = "c2r")
public class Receiver {

    //autowired the StudentService class
    @Autowired
    ClientService clientService;

    @Autowired
    Client client;

    @Autowired
    Sender sender;

    @RabbitHandler
    public void receive(String message) {
        String[] buffer = message.split("-");

        client.setId(parseInt(buffer[0]));
        client.setTrip(buffer[1]);
        client.setName(buffer[2]);
        client.setEmail(buffer[3]);

        clientService.saveOrUpdate(client);
        System.out.println(" [x] Received '" +
                client.getId()+"-"+
                client.getTrip()+"-"+
                client.getName()+"-"+
                client.getEmail()+"'");

        sender.send("created field with {" +
                "id: " + client.getId()+
                ", trip: " + client.getTrip()+
                ", name: " + client.getName()+
                ", email: " + client.getEmail());
    }
}
