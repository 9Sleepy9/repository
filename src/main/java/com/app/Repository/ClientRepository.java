package com.app.Repository;

import org.springframework.data.repository.CrudRepository;
import com.app.Domain.Client;

public interface ClientRepository extends CrudRepository<Client, Integer>
{
}

